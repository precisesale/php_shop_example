<?php

/**
 * Function that give true with probability from first argument
 *
 * @param float $probability
 * @param int $length
 * @return bool
 */
function trueWithProbability($probability=0.1, $length=10000)
{
    $test = mt_rand(1, $length);
    return $test<=$probability*$length;
}

/**
 * Selling in different price level
 *
 * // in 0 -> 0.5              |---
 * // between 0 and 20 linear  |    ---
 * // in 20 -> 0               |        ---
 * // greater than 20 -> 0     |            ---------
 *
 * @param $p
 * @return double
 */
function s($p)
{
    return max(0.0, 0.5 - 1/40 * $p);
}

const SHOP_URL = "http://localhost:8020";

$visits = 0;
$buys = 0;

for($i=0; $i<100; $i++) {

    $page = file_get_contents(SHOP_URL);

    preg_match("/<span>(\d+\.?\d*)<\/span>/", $page, $matches);

    $price = (float)$matches[1];

    $visits++;
    echo "Visits $visits\t Buys: $buys\t Price: ".$price."\n";

    if (trueWithProbability(s($price))) {
        file_get_contents(SHOP_URL . "/buy");
        $buys++;
    }

}