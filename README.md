# simple php shop 

Example of integration with https://precise.sale

Install dependencies

    sudo apt install php-json 
    composer install
    
Create .env file

    cp .env.dist .env
    
And fill .env with your API KEY form https://precise.sale
Start server with shop

    php -S localhost:8020
    
And run client simulator

    php simulate_visitors.php