<?php

require_once __DIR__ . '/Api.php';
require_once __DIR__ . '/Helpers.php';

class Controller
{
    static function index() {
        $state = Helpers::loadState();
        $state["visits"]++;

        $apiClient = new Api();

        if($state["visits"] % 10 === 0) { $state["price"] = $apiClient->reprice($state["price"]); }
        $apiClient->logVisit();

        Helpers::saveState($state);

        return Helpers::render('index',$state);
    }

    static function buy() {
        $state = Helpers::loadState();

        (new Api())->logBuy($state);

        return Helpers::render('confirm',$state);
    }
}