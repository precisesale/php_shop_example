<?php

const BASH_RED = "\e[0;31m";
const BASH_END = "\033[0m";

class Api
{
    private $URL;
    private $KEY;
    private $client;

    public function __construct()
    {
        $this->URL=getenv('PRECISE_SALE_URL');
        $this->KEY=getenv('PRECISE_SALE_API_KEY');
        $this->client = new GuzzleHttp\Client();
    }

    /**
     * @param $currentPrice double
     * @return double
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function reprice($currentPrice) {
        $res = $this->client->request('GET', $this->URL.'/product?to_reprice=true', [
            'headers' => [
                'Authorization' => 'Bearer '.$this->KEY,
                'Content-Type' => 'application/json'
            ]
        ]);

        $propositions = json_decode((string) $res->getBody());

        if(count($propositions) < 1) {
            file_put_contents("php://stdout", "Reprice finished without changes."."\n");
            return $currentPrice;
        } else {

            $price = $propositions[0]->proposed_price;
            $id = 1;

            $data = [
                "id" => $id,
                "price" => $price
            ];

            $res = $this->client->request('PUT', $this->URL.'/product/1', [
                'json' => $data,
                'headers' => [
                    'Authorization' => 'Bearer '.$this->KEY,
                ]
            ]);

            if($res->getStatusCode() !== 200) {
                file_put_contents("php://stdout", BASH_RED."Invalid confirmation that price is changed.".BASH_END."\n");
                return $currentPrice;
            } else {
                return $price;
            }
        }
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function logVisit() {

        $data = [
            "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
            "ip" => $_SERVER["REMOTE_ADDR"],
            "userAgent" => $_SERVER["HTTP_USER_AGENT"] ?? null,
            "productId" => 1
        ];

        $res = $this->client->request('POST', $this->URL.'/visit', [
            'json' => $data,
            'headers' => [
                'Authorization' => 'Bearer '.$this->KEY
            ]
        ]);

        if($res->getStatusCode() !== 201) {
            file_put_contents("php://stdout", BASH_RED."Invalid visit logging. Check URL and API KEY in .env file.".BASH_END."\n");
        }
    }

    /**
     * @param $state
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function logBuy($state) {
        $id = random_int(1, 1e10);

        $data = [
            "id" => $id,
            "grand_total" => $state["price"],
            "products" => [[
                "id" =>  1,
                "price" => $state["price"],
                "quantity" => 1
            ]],
        ];

        $res = $this->client->request('PUT', $this->URL.'/order/'.$id, [
            'json' => $data,
            'headers' => [
                'Authorization' => 'Bearer '.$this->KEY
            ]
        ]);

        if($res->getStatusCode() !== 200) {
            file_put_contents("php://stdout", BASH_RED."Invalid buy logging. Check URL and API KEY in .env file.".BASH_END."\n");
        }
    }
}