<?php

class Helpers
{
    const DB = __DIR__ . '/../db.json';

    static function render($file, $data) {
        $text = file_get_contents(__DIR__ . '/../template/' . $file . '.html');
        foreach ($data as $key => $val) {
            $text = preg_replace('/{{'.$key.'}}/',$val, $text );
        }
        return $text;
    }

    static function loadState() {
        if(file_exists(self::DB)) {
            $json = file_get_contents(self::DB);
        } else {
            $default = '{"price":3,"visits":0}';
            file_put_contents(self::DB, $default);
            $json = $default;
        }
        return json_decode($json,true);
    }

    static function saveState($state) {
        file_put_contents(self::DB, json_encode($state));
    }
}