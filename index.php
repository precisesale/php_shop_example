<?php

require_once 'src/Controller.php';
require_once 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$path = $_SERVER["REQUEST_URI"];

if($path === "/buy") {
    echo Controller::buy();
} else {
    echo Controller::index();
}
